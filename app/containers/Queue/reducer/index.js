import {combineReducers} from 'redux';
import dataReducer from './data';
import organizationReducer from './organization';

export default combineReducers({data: dataReducer, organization: organizationReducer});