/*
 *
 * Queue
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectQueue from './selectors';
import {fetchMultipleQueueItems, fetchQueueLabels} from './actions';
import {makeDatesCollection} from 'helpers/collections/date';
import Container from 'components/Container';
import QueueItemList from 'containers/QueueItemList';
import {COLLECT_BY_LABEL, COLLECT_BY_DATE} from './constants';
import {getCollection} from 'helpers/collections';

export class Queue extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount(){
    this.props.dispatch(fetchMultipleQueueItems(0));
    this.props.dispatch(fetchQueueLabels(0));
  }

  render() {
    const {Queue:{data:{queueLabels, queueItemsData},
                 organization:{collectBy, sortBy}}} = this.props;

    return (
      <Container>
        <div>
          {getCollection(collectBy, {labels: queueLabels, items: queueItemsData, sortBy})}
        </div>
      </Container>
    );
  }
}

Queue.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Queue: makeSelectQueue(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Queue);
