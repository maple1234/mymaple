import { take, call, put, select, takeEvery, takeLatest } from 'redux-saga/effects';
import {
  SET_ITEM_IS_READ,
  SET_ITEM_IS_UNREAD,
  SET_ITEM_IS_DONE,
  SET_ITEM_IS_UNDONE,
  SET_ITEM_IS_PINNED,
  SET_ITEM_IS_UNPINNED
} from './constants';
import {
  setItemIsReadSuccess,
  setItemIsReadFailed,
  setItemIsUnReadSuccess,
  setItemIsUnReadFailed,
  setItemIsDoneSuccess,
  setItemIsDoneFailed,
  setItemIsUnDoneSuccess,
  setItemIsUnDoneFailed,
  setItemIsPinnedSuccess,
  setItemIsPinnedFailed,
  setItemIsUnPinnedSuccess,
  setItemIsUnPinnedFailed
} from './actions';
import {setSomething} from './mock';

function setItemStatus(success, fail){
  return function* (action){
    try{
      const result = yield call(setSomething, action.externalId);
      result ? yield put(success(action.externalId)): yield put(fail(result));
    } catch(e){
      yield put(fail(e));
    }
  }
}

export function* defaultSaga() {
  yield takeEvery(SET_ITEM_IS_READ, setItemStatus(setItemIsReadSuccess, setItemIsReadFailed));
  yield takeEvery(SET_ITEM_IS_UNREAD, setItemStatus(setItemIsUnReadSuccess, setItemIsUnReadFailed));
  yield takeEvery(SET_ITEM_IS_DONE, setItemStatus(setItemIsDoneSuccess, setItemIsDoneFailed));
  yield takeEvery(SET_ITEM_IS_UNDONE, setItemStatus(setItemIsUnDoneSuccess, setItemIsUnDoneFailed));
  yield takeEvery(SET_ITEM_IS_PINNED, setItemStatus(setItemIsPinnedSuccess, setItemIsPinnedFailed));
  yield takeEvery(SET_ITEM_IS_UNPINNED, setItemStatus(setItemIsUnPinnedSuccess, setItemIsUnPinnedFailed));
}

// All sagas to be loaded
export default [
  defaultSaga,
];
