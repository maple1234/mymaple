
import { fromJS } from 'immutable';
import queueItemReducer from '../reducer';

describe('queueItemReducer', () => {
  it('returns the initial state', () => {
    expect(queueItemReducer(undefined, {})).toEqual(fromJS({}));
  });
});
