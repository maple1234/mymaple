import React from 'react';
import styled from 'styled-components';
import {fadeIn} from './Animations';

export const Details = styled.div`
    display: none;
    line-height: 1.5;
    width: 100%;
`;

export const DetailsOpening = styled(Details)`
    display: block;
    position: absolute;
    left: -50000px;
    width: 100%;
`;

export const DetailsOpen = styled(Details)`
    display: block;
    position: static;
    opacity: 0.0001;
    animation-name: ${fadeIn};
    animation-duration: 150ms;
    animation-fill-mode: forwards;
`;

export const DetailsHeader = styled.div`
    display: flex;
    align-items: center;
    background: #fff;
    cursor: pointer;
    padding: 16px 20px;
`;

export const DetailsHeaderFixed = styled(DetailsHeader)`
    position: fixed;
    top: 0;
`;

export const DetailsHeaderAbsolute = styled(DetailsHeader)`
    position: absolute;
    bottom: 0;
`;

export const DetailsShadow = styled.div`
    bottom: 0;
    box-shadow: 0 -1px 0 #e5e5e5, 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 4px rgba(0, 0, 0, 0.24);
    content: '';
    display: block;
    left: 0;
    pointer-events: none;
    position: absolute;
    right: 0;
    top: 0;
`;

export const DetailsTitle = styled.div`
    line-height: 1;
    margin: 0;
`;

export const DetailsBody = styled.div`
    border-top: 1px solid #e5e5e5;
`;

export const DetailsIcon = styled.div`
    float: left;
    width: 40px;
    height: 40px;
    margin: 16px 20px;
`;

export const DetailsContent = styled.div`
    margin: 0 20px 16px 80px;
    padding: 0 16px 16px;
    text-align: justify;
`;