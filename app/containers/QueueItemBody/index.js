/*
 *
 * QueueItemBody
 *
 */

import React, { PropTypes } from 'react';
import ItemFrame from 'components/ItemFrame';
import { connect } from 'react-redux';
import classNames from 'classnames';

export class QueueItemBody extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {queueItem, isOpened} = this.props;
    const classes = classNames({"hide": !isOpened});
    return (
        <div className={classes}>
            <ItemFrame externalId={queueItem.externalId} 
                        isVisible={isOpened}
                        src={`https://www.fxp.co.il/showthread.php?t=${queueItem.externalId}`}/>
        </div>
      );
    }
}

QueueItemBody.propTypes = {
  dispatch: PropTypes.func.isRequired,
  queueItem: PropTypes.object.isRequired,
  isOpened: PropTypes.bool.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(QueueItemBody);
