/*
 *
 * Header
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectHeader from './selectors';
import AppBar from 'material-ui/AppBar';

export default class Header extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <header>
        <AppBar title="מייפל" iconClassNameRight="muidocs-icon-navigation-expand-more" /> {/*more_vert*/}
      </header>
    );
  }
}

// Header.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// const mapStateToProps = createStructuredSelector({
//   Header: makeSelectHeader(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// export default connect(mapStateToProps, mapDispatchToProps)(Header);
