/*
 *
 * QueueItemList
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectQueueItemList from './selectors';
import {getSort} from 'helpers/sorts';
import {List} from './styles';
import Card from 'material-ui/Card';
import QueueItem from 'containers/QueueItem';
import QueueItemListHeader from 'containers/QueueItemListHeader';

export class QueueItemList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {queueListItemsData, sortBy} = this.props;
    
    return (
      <div>
        <QueueItemListHeader listName={this.props.listName}/>
        <Card>
          <List>
            {getSort(sortBy, {items: queueListItemsData, dec: true})}
          </List>
        </Card>
      </div>
    );
  }
}

QueueItemList.propTypes = {
  dispatch: PropTypes.func.isRequired,
  queueListItemsData: PropTypes.array.isRequired,
  listName: PropTypes.string.isRequired,
  sortBy: PropTypes.string.isRequired
};

const mapStateToProps = createStructuredSelector({
  QueueItemList: makeSelectQueueItemList(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QueueItemList);
