import _ from 'lodash';
import React from 'React';
import QueueItemList from 'containers/QueueItemList';
import {makeDatesCollection} from './date';
import {makeLabelsCollection} from './label';
import {COLLECT_BY_DATE, COLLECT_BY_LABEL} from 'containers/Queue/constants';

export function getCollection(type, data){
    switch(type){
        case COLLECT_BY_LABEL: 
            return setCollectionForLabel(data);
        case COLLECT_BY_DATE:
            return setCollectionForDate(data);
        default:
            return null;
    }
}

function setCollectionForDate(data){
    const collection = makeDatesCollection(data.items);
    return _.map(collection, ((value, prop)=><QueueItemList key={prop} queueListItemsData={value.data} listName={prop} sortBy={data.sortBy}/>))
}

function setCollectionForLabel(data){
    const collection = makeLabelsCollection(data.items, data.labels);
    return _.map(collection, ((value)=><QueueItemList key={value.label.labelId} queueListItemsData={value.data} listName={value.label.name} sortBy={data.sortBy}/>));
}