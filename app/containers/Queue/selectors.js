import { createSelector } from 'reselect';
import { fromJS } from 'immutable';

/**
 * Direct selector to the queue state domain
 */
const selectQueueDomain = () => (state) => fromJS(state.get('queue'));

/**
 * Other specific selectors
 */


/**
 * Default selector used by Queue
 */

const makeSelectQueue = () => createSelector(
  selectQueueDomain(),
  (substate) => substate.toJS()
);

export default makeSelectQueue;
export {
  selectQueueDomain,
};
