import { createSelector } from 'reselect';

/**
 * Direct selector to the queueItemHeader state domain
 */
const selectQueueItemHeaderDomain = () => (state) => state.get('queueItemHeader');

/**
 * Other specific selectors
 */


/**
 * Default selector used by QueueItemHeader
 */

const makeSelectQueueItemHeader = () => createSelector(
  selectQueueItemHeaderDomain(),
  (substate) => substate.toJS()
);

export default makeSelectQueueItemHeader;
export {
  selectQueueItemHeaderDomain,
};
