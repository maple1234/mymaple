import { fromJS } from 'immutable';
import {
    COLLECT_BY_DATE,
    COLLECT_BY_LABEL,
    SORT_BY_DATE
  } from '../constants';

const initialState = fromJS({ collectBy: COLLECT_BY_DATE, sortBy: SORT_BY_DATE});

function organizationReducer(state = initialState, action){
    switch (action.type) {
        case COLLECT_BY_DATE:
            return state.set("collectBy", COLLECT_BY_DATE);
        case COLLECT_BY_LABEL:
            return state.set("collectBy", COLLECT_BY_LABEL);
        case SORT_BY_DATE:
            return state.set("sortBy", SORT_BY_DATE);
        default: 
            return state;
  }
}

export default organizationReducer;