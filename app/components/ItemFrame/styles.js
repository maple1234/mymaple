import React from 'react';
import styled from 'styled-components';

export const IFrame = styled.iframe`
    width: 100%;
    height: 100%;
`;