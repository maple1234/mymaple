/*
 *
 * QueueItemListHeader
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import { ListHeader, Label, FinishIconContainer } from './styles';

export class QueueItemListHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {listName} = this.props;

    return (
      <ListHeader>
        <Label>{listName}</Label>
        <FinishIconContainer className="left">
          <IconButton iconClassName="mdi mdi-check-all"/>
        </FinishIconContainer>
      </ListHeader>
    );
  }
}

QueueItemListHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  listName: PropTypes.string.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(QueueItemListHeader);
