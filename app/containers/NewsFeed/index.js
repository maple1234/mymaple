/*
 *
 * NewsFeed
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectNewsFeed from './selectors';
import Queue from 'containers/Queue';

export default class NewsFeed extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div style={{direction:"rtl"}}>
        <Queue />
      </div>
    );
  }
}

// NewsFeed.propTypes = {
//   dispatch: PropTypes.func.isRequired,
// };

// const mapStateToProps = createStructuredSelector({
//   NewsFeed: makeSelectNewsFeed(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

// export default connect(mapStateToProps, mapDispatchToProps)(NewsFeed);
