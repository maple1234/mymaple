/*
 *
 * QueueItemHeader
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectQueueItemHeader from './selectors';
import {ListItem} from './styles';
import {closeItem, openItem, setItemIsRead} from 'containers/QueueItem/actions';
import {showToolkit, hideToolkit} from './actions';
import QueueItemToolkit from 'containers/QueueItemToolkit';

export class QueueItemHeader extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props, context){
    super(props, context);
    this.showToolkit = this.showToolkit.bind(this);
    this.hideToolkit = this.hideToolkit.bind(this);
    this.rowClicked = this.rowClicked.bind(this);
  }

  showToolkit(){
    const {dispatch, queueItem} = this.props;
    dispatch(showToolkit(queueItem.externalId));
  }

  hideToolkit(){
    const {dispatch, queueItem} = this.props;
    dispatch(hideToolkit(queueItem.externalId));
  }

  rowClicked(){
    const {dispatch, queueItem, isOpened} = this.props;

    if(isOpened){
      dispatch(closeItem());
    } else{
      dispatch(openItem(queueItem.externalId));
      if(!queueItem.isRead){
        dispatch(setItemIsRead(queueItem.externalId));
      }
    }
  }

  render() {
    const {queueItem, isOpened, QueueItemHeader: {toolkitVisible}} = this.props;
    const isToolkitShown = !!toolkitVisible[queueItem.externalId];

    return (
      <div onMouseOver={this.showToolkit} onMouseLeave={this.hideToolkit}>
        <QueueItemToolkit isVisible={(isToolkitShown || isOpened)}/>
        <ListItem onClick={this.rowClicked}>
            <div style={{fontWeight: `${!queueItem.isRead ? "bold" : "initial"}`}}>{ queueItem.title }</div>
        </ListItem>
      </div>
    );
  }
}

QueueItemHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isOpened: PropTypes.bool.isRequired,
  queueItem: PropTypes.object.isRequired
};

const mapStateToProps = createStructuredSelector({
  QueueItemHeader: makeSelectQueueItemHeader(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QueueItemHeader);
