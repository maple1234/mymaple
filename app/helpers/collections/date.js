import moment from 'moment';
import {Map, fromJS} from 'immutable';

export const UNKNOWN = "לא ידוע";
export const TODAY = "היום";
export const YESTERDAY = "אתמול";
export const TWO_DAYS_AGO = "לפני יומיים";
export const THREE_DAYS_AGO = "לפני שלושה ימים";
export const FOUR_DAYS_AGO = "לפני ארבעה ימים";
export const FIVE_DAYS_AGO = "לפני חמישה ימים";
export const SIX_DAYS_AGO = "לפני שישה ימים";
export const WEEK_AGO = "לפני שבוע";
export const MORE_THAN_WEEK_AGO = "לפני יותר משבוע";
const format = "DD/MM/YYYY HH:mm";

export function makeDatesCollection(items=[]){
    const now = moment(new Date(), format);
    const allItemDates = collectItems(now, items);
    return sortByPosition(allItemDates).toJS();
}

function sortByPosition(allItemDates){
    return allItemDates.sort((a, b)=>{
        return a.get("position") > b.get("position");
    });
}

function collectItems(now, items){
    let allItemDates = Map();
    items.map((item)=>{
        allItemDates = setItemInCollection(now, item, allItemDates);        
    });
    return allItemDates;
}

function setItemInCollection(now, item, collection){
    const days = getDiffrenceInDays(now, moment(item.date, format));
    const {word, position} = getSingleDateObject(days);
    return collection.has(word) ? collection.setIn([word, 'data'], collection.getIn([word, 'data']).push(fromJS(item))) :
                                  collection.set(word, fromJS({data: [item], position}));
}

function getDiffrenceInDays(start, end) {
    return -end.diff(start, "days");
}

function getSingleDateObject(amountOfDays){
    switch(amountOfDays){
        case 0: return setSingleDateObject(TODAY, amountOfDays);
        case 1: return setSingleDateObject(YESTERDAY, amountOfDays);
        case 2: return setSingleDateObject(TWO_DAYS_AGO, amountOfDays);
        case 3: return setSingleDateObject(THREE_DAYS_AGO, amountOfDays);
        case 4: return setSingleDateObject(FOUR_DAYS_AGO, amountOfDays);
        case 5: return setSingleDateObject(FIVE_DAYS_AGO, amountOfDays);
        case 6: return setSingleDateObject(SIX_DAYS_AGO, amountOfDays);
        case 7: return setSingleDateObject(WEEK_AGO, amountOfDays);
        default: return amountOfDays > 7 ? setSingleDateObject(MORE_THAN_WEEK_AGO, 8) : setSingleDateObject(UNKNOWN, -1);
    }
}

function setSingleDateObject(word, position){
    return {word, position};
}