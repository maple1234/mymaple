/*
 *
 * QueueItem
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import makeSelectQueueItem from './selectors';
import QueueItemHeader from 'containers/QueueItemHeader';
import QueueItemBody from 'containers/QueueItemBody';

export class QueueItem extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {queueItem, QueueItem: {openedItemExternalId}} = this.props;
    const isOpened = openedItemExternalId === queueItem.externalId;    
    return (
      <div>
        <QueueItemHeader isOpened={isOpened} queueItem={queueItem}/>
        <QueueItemBody isOpened={isOpened} queueItem={queueItem} />
      </div>
    );
  }
}

QueueItem.propTypes = {
  dispatch: PropTypes.func.isRequired,
  queueItem: PropTypes.object.isRequired
};

const mapStateToProps = createStructuredSelector({
  QueueItem: makeSelectQueueItem(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(QueueItem);
