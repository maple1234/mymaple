/*
 *
 * Queue actions
 *
 */

import {
  FETCH_MULTIPLE_QUEUE_ITEMS,
  FETCH_MULTIPLE_QUEUE_ITEMS_SUCCESS,
  FETCH_MULTIPLE_QUEUE_ITEMS_FAILED,
  FETCH_QUEUE_LABELS,
  FETCH_QUEUE_LABELS_SUCCESS,
  FETCH_QUEUE_LABELS_FAILED
} from './constants';

export function fetchMultipleQueueItems(queueId) {
  return {
    type: FETCH_MULTIPLE_QUEUE_ITEMS,
    queueId
  };
}

export function fetchMultipleQueueItemsSuccess(multipleQueueItems) {
  return {
    type: FETCH_MULTIPLE_QUEUE_ITEMS_SUCCESS,
    multipleQueueItems
  };
}

export function fetchMultipleQueueItemsFailed(err) {
  return {
    type: FETCH_MULTIPLE_QUEUE_ITEMS_FAILED,
    err
  };
}

export function fetchQueueLabels(queueId) {
  return {
    type: FETCH_QUEUE_LABELS,
    queueId
  };
}

export function fetchQueueLabelsSuccess(queueLabels) {
  return {
    type: FETCH_QUEUE_LABELS_SUCCESS,
    queueLabels
  };
}

export function fetchQueueLabelsFailed(err) {
  return {
    type: FETCH_QUEUE_LABELS_FAILED,
    err
  };
}