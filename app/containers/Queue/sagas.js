import { take, call, put, select, takeLatest, takeEvery } from 'redux-saga/effects';
import {
  FETCH_MULTIPLE_QUEUE_ITEMS,
  FETCH_QUEUE_LABELS
  } from './constants';
import {
  fetchMultipleQueueItemsSuccess,
  fetchMultipleQueueItemsFailed,
  fetchQueueLabelsSuccess,
  fetchQueueLabelsFailed
  } from './actions';
import {multipile, getLabels} from './mock';

function* fetchMultipleQueueItem(action){
   try {
      const multipleQueueItems = yield call(multipile, action.queueId);
      yield put(fetchMultipleQueueItemsSuccess(multipleQueueItems));
   } catch (err) {
      yield put(fetchMultipleQueueItemsFailed(err));
   }
}

function* fetchQueueLabels(action){
  try{
    const queueLabels = yield call(getLabels, action.queueId);
    yield put(fetchQueueLabelsSuccess(queueLabels));
  } catch(err){
    yield put(fetchQueueLabelsFailed(err));
  }
}

function* defaultSaga() {
  yield takeEvery(FETCH_MULTIPLE_QUEUE_ITEMS, fetchMultipleQueueItem);
  yield takeEvery(FETCH_QUEUE_LABELS, fetchQueueLabels);
}

export default [
  defaultSaga
];
