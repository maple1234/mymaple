import moment from 'moment';

export function sortByDate(items=[], dec=true){
    const date = moment();
    const format = "DD/MM/YYYY HH:mm:ss";

    return items.sort((a, b)=>{
        const dateA = moment(a.date, format);
        const dateB = moment(b.date, format);
        return dec ? dateA < dateB : dateA > dateB;
    });
}