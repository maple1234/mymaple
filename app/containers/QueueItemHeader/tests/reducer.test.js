
import { fromJS } from 'immutable';
import queueItemHeaderReducer from '../reducer';

describe('queueItemHeaderReducer', () => {
  it('returns the initial state', () => {
    expect(queueItemHeaderReducer(undefined, {})).toEqual(fromJS({}));
  });
});
