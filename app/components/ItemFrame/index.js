/**
*
* ItemFrame
*
*/

import React from 'react';
import {IFrame} from './styles';

function ItemFrame({externalId, src, isVisible}) {
  return (
    <IFrame id={ "iFrameExternalId-" + externalId } src={isVisible ? src : ""} frameBorder="0">
    </IFrame>
  );
}

ItemFrame.propTypes = {
  externalId: React.PropTypes.number.isRequired,
  src: React.PropTypes.string.isRequired,
  isVisible: React.PropTypes.bool.isRequired
};

export default ItemFrame;
