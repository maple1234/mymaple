import React from 'react';
import styled from 'styled-components';
import {fadeIn, fadeOut} from './Animations';

export const Summary = styled.div`
    display: flex;
    cursor: pointer;
    align-items: center;
    width: 100%;
`;

export const SummaryOpenning = styled(Summary)`
    animation: ${fadeOut} 150ms forwards;
`;

export const SummaryClosing = styled(Summary)`
    display: flex;
    opacity: 0;
    animation-name: ${fadeIn};
    animation-duration: 150ms;
    animation-fill-mode: forwards;
`;

export const SummaryOpen = styled(Summary)`
    display: none;
`;

export const SummaryIcon = styled.div`
    padding: 10px 22px;
`;

export const SummaryBody = styled.div`
    padding-left: 16px;
    color: gray;
`;

export const SummaryTitle = styled.div`
`;

export const SummaryShadow = styled.div`
    bottom: 0;
    box-shadow: 0 -1px 0 #e5e5e5, 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 4px rgba(0, 0, 0, 0.24);
    content: '';
    display: block;
    left: 0;
    pointer-events: none;
    position: absolute;
    right: 0;
    top: 0;
`;

export const SummaryShadowOpenning = styled(SummaryShadow)`
    display: none;
`;

export const SummaryShadowClosing = styled(SummaryShadow)`
    display: block;
    opacity: 0;
    animation-name: ${fadeIn};
    animation-duration: 150ms;
    animation-fill-mode: forwards;
`;