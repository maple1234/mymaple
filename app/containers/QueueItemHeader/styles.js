import React from 'react';
import styled from 'styled-components';
import {ListItem as MaterialListItem} from 'material-ui/List';

export const ListItem = styled(MaterialListItem)`
 min-height:90px;
`;