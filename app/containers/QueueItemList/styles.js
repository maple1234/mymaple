import React from 'react';
import styled from 'styled-components';
import {List as MaterialList} from 'material-ui/List';

export const List = styled(MaterialList)`
 padding: 0 !important;
`;