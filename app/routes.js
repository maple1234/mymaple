// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Queue/reducer'),
          import('containers/Queue/sagas'),
          import('containers/QueueItem/reducer'),
          import('containers/QueueItem/sagas'),
          import('containers/QueueItemHeader/reducer'),
          import('containers/QueueItemHeader/sagas'),
          import('containers/QueueItemList/reducer'),
          import('containers/QueueItemList/sagas'),
          import('containers/HomePage')
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([queueReducer, queueSagas,
                             queueItemReducer, queueItemSagas,
                             queueItemHeaderReducer, queueItemHeaderSagas,
                             queueItemListReducer, queueItemListSagas,
                             homePage]) => {
          injectReducer('queueReducer', queueReducer.default);
          injectSagas(queueSagas.default);
          injectReducer('queueItemReducer', queueItemReducer.default);
          injectSagas(queueItemSagas.default);
          injectReducer('queueItemHeaderReducer', queueItemHeaderReducer.default);
          injectSagas(queueItemHeaderSagas.default);
          injectReducer('queueItemListReducer', queueItemListReducer.default);
          injectSagas(queueItemListSagas.default);
          renderRoute(homePage);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
