/*
 *
 * QueueItem actions
 *
 */
import {changeOverallStatus, changeItemStatusAction, changeItemStatusActionFailed} from 'helpers/itemStatusHelper';
import {
  SHOW_TOOLKIT,
  HIDE_TOOLKIT
} from './constants';

export const showToolkit = changeItemStatusAction(SHOW_TOOLKIT);
export const hideToolkit = changeItemStatusAction(HIDE_TOOLKIT);