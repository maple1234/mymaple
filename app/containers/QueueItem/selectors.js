import { createSelector } from 'reselect';

/**
 * Direct selector to the queueItem state domain
 */
const selectQueueItemDomain = () => (state) => state.get('queueItem');

/**
 * Other specific selectors
 */


/**
 * Default selector used by QueueItem
 */

const makeSelectQueueItem = () => createSelector(
  selectQueueItemDomain(),
  (substate) => substate.toJS()
);

export default makeSelectQueueItem;
export {
  selectQueueItemDomain,
};
