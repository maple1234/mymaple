import React from 'react';
import styled from 'styled-components';

export const ListHeader = styled.div`
  margin: 24px 0px 12px 0px;
  padding: 0 24px;
`;

export const Label = styled.span`
 fontSize: 81.25%;
`;

export const FinishIconContainer = styled.div`
 margin-top: -14px;
 float: left;
`;
