/*
 *
 * QueueItem constants
 *
 */

export const SHOW_TOOLKIT = 'app/QueueItemHeader/SHOW_TOOLKIT';
export const HIDE_TOOLKIT = 'app/QueueItemHeader/HIDE_TOOLKIT';