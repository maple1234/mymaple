/*
 *
 * QueueItem reducer
 *
 */

import { fromJS } from 'immutable';
import {
  OPEN_ITEM,
  CLOSE_ITEM
} from './constants';

const initialState = fromJS({ openedItemExternalId: null });

function queueItemReducer(state = initialState, action) {
  switch (action.type) {
    case OPEN_ITEM: 
      return state.set('openedItemExternalId', action.externalId);
    case CLOSE_ITEM:
      return state.set('openedItemExternalId', null);
    default:
      return state;
  }
}

export default queueItemReducer;
