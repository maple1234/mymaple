
import { fromJS } from 'immutable';
import queueItemListHeaderReducer from '../reducer';

describe('queueItemListHeaderReducer', () => {
  it('returns the initial state', () => {
    expect(queueItemListHeaderReducer(undefined, {})).toEqual(fromJS({}));
  });
});
