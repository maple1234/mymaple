import React from 'react';
import {keyframes} from 'styled-components';

export const fadeOut = keyframes`
  0%,33% {
    opacity: 0.9999;
  }
  100% {
    opacity: 0.0001;
  }
`;


export const fadeIn = keyframes`
  0%,33% {
    opacity: 0.0001;
  }
  100% {
    opacity: 0.9999;
  }
`;

export const shrink = keyframes`
  to {
    height: 0;
  }
`;