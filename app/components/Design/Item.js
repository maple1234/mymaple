import React from 'react';
import styled from 'styled-components';
import {shrink} from './Animations';

export const Item = styled.div`
    background: #fff;
    margin-bottom: 20px;
    transition: margin 150ms;
    position: relative;
`;

export const ItemOpening = styled.div`
    margin-left: -20px;
    margin-right: -20px;
`;

export const ItemOpen = styled.div`
    margin-left: -20px;
    margin-right: -20px;
`;

export const ItemIcon = styled.div`
    height: 28px;
    width: 28px;
    border-radius: 14px;
    background: seagreen;
`;

export const Pusher = styled.div`
    display: none;
`;

export const PusherBlock = styled(Pusher)`
    display: block;
`;

export const Grower = styled.div`
    height: 0;
    background: #fff;
    will-change: height;
    transition: height 150ms;
`;

export const GrowerOpen = styled(Grower)`
    display: none;
`;

export const GrowerClosing = styled(Grower)`
    animation: ${shrink} 37.5ms forwards;
`;