
export const OPEN_ITEM = 'app/QueueItem/OPEN_ITEM';
export const CLOSE_ITEM = 'app/QueueItem/CLOSE_ITEM';

export const SET_ITEM_IS_READ = 'app/QueueItem/SET_ITEM_IS_READ';
export const SET_ITEM_IS_READ_SUCCESS = 'app/QueueItem/SET_ITEM_IS_READ_SUCCESS';
export const SET_ITEM_IS_READ_FAILED = 'app/QueueItem/SET_ITEM_IS_READ_FAILED';

export const SET_ITEM_IS_UNREAD = 'app/QueueItem/SET_ITEM_IS_UNREAD';
export const SET_ITEM_IS_UNREAD_SUCCESS = 'app/QueueItem/SET_ITEM_IS_UNREAD_SUCCESS';
export const SET_ITEM_IS_UNREAD_FAILED = 'app/QueueItem/SET_ITEM_IS_UNREAD_FAILED';

export const SET_ITEM_IS_DONE = 'app/QueueItem/SET_ITEM_IS_DONE';
export const SET_ITEM_IS_DONE_SUCCESS = 'app/QueueItem/SET_ITEM_IS_DONE_SUCCESS';
export const SET_ITEM_IS_DONE_FAILED = 'app/QueueItem/SET_ITEM_IS_DONE_FAILED';

export const SET_ITEM_IS_UNDONE = 'app/QueueItem/SET_ITEM_IS_UNDONE';
export const SET_ITEM_IS_UNDONE_SUCCESS = 'app/QueueItem/SET_ITEM_IS_UNDONE_SUCCESS';
export const SET_ITEM_IS_UNDONE_FAILED = 'app/QueueItem/SET_ITEM_IS_UNDONE_FAILED';

export const SET_ITEM_IS_PINNED = 'app/QueueItem/SET_ITEM_IS_PINNED';
export const SET_ITEM_IS_PINNED_SUCCESS = 'app/QueueItem/SET_ITEM_IS_PINNED_SUCCESS';
export const SET_ITEM_IS_PINNED_FAILED = 'app/QueueItem/SET_ITEM_IS_PINNED_FAILED';

export const SET_ITEM_IS_UNPINNED = 'app/QueueItem/SET_ITEM_IS_UNPINNED';
export const SET_ITEM_IS_UNPINNED_SUCCESS = 'app/QueueItem/SET_ITEM_IS_UNPINNED_SUCCESS';
export const SET_ITEM_IS_UNPINNED_FAILED = 'app/QueueItem/SET_ITEM_IS_UNPINNED_FAILED';
