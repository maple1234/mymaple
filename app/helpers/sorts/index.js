import React from 'React';
import _ from 'lodash';
import {SORT_BY_DATE} from 'containers/Queue/constants';
import {sortByDate} from './date';
import QueueItem from 'containers/QueueItem';
import Divider from 'material-ui/Divider';

export function getSort(type, data){
    switch(type){
        case SORT_BY_DATE: 
            return setSortForDate(data);
        default:
            return null;
    }
}

function setSortForDate(data){
    const sort = sortByDate(data.items, data.dec);
    return _.map(sort, ((queueItem)=>[<Divider />, <QueueItem key={queueItem.externalId} queueItem={queueItem} />]))
}