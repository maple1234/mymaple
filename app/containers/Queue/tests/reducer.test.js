
import { fromJS } from 'immutable';
import queueReducer from '../reducer';

describe('queueReducer', () => {
  it('returns the initial state', () => {
    expect(queueReducer(undefined, {})).toEqual(fromJS({}));
  });
});
