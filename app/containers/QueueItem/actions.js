/*
 *
 * QueueItem actions
 *
 */
import {changeItemStatusAction, changeItemStatusActionFailed} from 'helpers/itemStatusHelper';
import {
  OPEN_ITEM,
  CLOSE_ITEM,
  /* Read */
  SET_ITEM_IS_READ,
  SET_ITEM_IS_READ_SUCCESS,
  SET_ITEM_IS_READ_FAILED,
  /* UnRead */
  SET_ITEM_IS_UNREAD,
  SET_ITEM_IS_UNREAD_SUCCESS,
  SET_ITEM_IS_UNREAD_FAILED,
  /* Done */
  SET_ITEM_IS_DONE,
  SET_ITEM_IS_DONE_SUCCESS,
  SET_ITEM_IS_DONE_FAILED,
  /* UnDone */
  SET_ITEM_IS_UNDONE,
  SET_ITEM_IS_UNDONE_SUCCESS,
  SET_ITEM_IS_UNDONE_FAILED,
  /* Pinned */
  SET_ITEM_IS_PINNED,
  SET_ITEM_IS_PINNED_SUCCESS,
  SET_ITEM_IS_PINNED_FAILED,
  /* UnPinned */
  SET_ITEM_IS_UNPINNED,
  SET_ITEM_IS_UNPINNED_SUCCESS,
  SET_ITEM_IS_UNPINNED_FAILED
} from './constants';

export const openItem = changeItemStatusAction(OPEN_ITEM);
export const closeItem = changeItemStatusAction(CLOSE_ITEM);

export const setItemIsRead = changeItemStatusAction(SET_ITEM_IS_READ);
export const setItemIsReadSuccess = changeItemStatusAction(SET_ITEM_IS_READ_SUCCESS);
export const setItemIsReadFailed = changeItemStatusActionFailed(SET_ITEM_IS_READ_FAILED);

export const setItemIsUnRead = changeItemStatusAction(SET_ITEM_IS_UNREAD);
export const setItemIsUnReadSuccess = changeItemStatusAction(SET_ITEM_IS_UNREAD_SUCCESS);
export const setItemIsUnReadFailed = changeItemStatusActionFailed(SET_ITEM_IS_UNREAD_FAILED);

export const setItemIsDone = changeItemStatusAction(SET_ITEM_IS_DONE);
export const setItemIsDoneSuccess = changeItemStatusAction(SET_ITEM_IS_DONE_SUCCESS);
export const setItemIsDoneFailed = changeItemStatusActionFailed(SET_ITEM_IS_DONE_FAILED);

export const setItemIsUnDone = changeItemStatusAction(SET_ITEM_IS_UNDONE);
export const setItemIsUnDoneSuccess = changeItemStatusAction(SET_ITEM_IS_UNDONE_SUCCESS);
export const setItemIsUnDoneFailed = changeItemStatusActionFailed(SET_ITEM_IS_UNDONE_FAILED);

export const setItemIsPinned = changeItemStatusAction(SET_ITEM_IS_PINNED);
export const setItemIsPinnedSuccess = changeItemStatusAction(SET_ITEM_IS_PINNED_SUCCESS);
export const setItemIsPinnedFailed = changeItemStatusActionFailed(SET_ITEM_IS_PINNED_FAILED);

export const setItemIsUnPinned = changeItemStatusAction(SET_ITEM_IS_UNPINNED);
export const setItemIsUnPinnedSuccess = changeItemStatusAction(SET_ITEM_IS_UNPINNED_SUCCESS);
export const setItemIsUnPinnedFailed = changeItemStatusActionFailed(SET_ITEM_IS_UNPINNED_FAILED);

