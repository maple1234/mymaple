/**
*
* Icon
*
*/

import React from 'react';
import classNames from 'classnames';
// import styled from 'styled-components';


function Icon({name, children, color}) {
  const classes = classNames(color,
                             {"material-icons": name});

  return (
    <i className={classes}>{name ? name : children}</i>
  );
}

Icon.defaultProps = {
  color: ""
};

Icon.propTypes = {
  name: React.PropTypes.string,
  color: React.PropTypes.string,
  children: React.PropTypes.node
};

export default Icon;
