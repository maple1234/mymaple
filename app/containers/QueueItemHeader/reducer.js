/*
 *
 * QueueItemHeader reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SHOW_TOOLKIT,
  HIDE_TOOLKIT
} from './constants';

const initialState = fromJS({ toolkitVisible: {} });

function queueItemHeaderReducer(state = initialState, action) {
  switch (action.type) {
    case SHOW_TOOLKIT:
      return state.setIn(['toolkitVisible', action.externalId], true);
    case HIDE_TOOLKIT:
      return state.setIn(['toolkitVisible', action.externalId], false);
    default:
      return state;
  }
}

export default queueItemHeaderReducer;
