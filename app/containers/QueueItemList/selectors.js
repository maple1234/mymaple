import { createSelector } from 'reselect';

/**
 * Direct selector to the queueItemList state domain
 */
const selectQueueItemListDomain = () => (state) => state.get('queueItemList');

/**
 * Other specific selectors
 */


/**
 * Default selector used by QueueItemList
 */

const makeSelectQueueItemList = () => createSelector(
  selectQueueItemListDomain(),
  (substate) => substate.toJS()
);

export default makeSelectQueueItemList;
export {
  selectQueueItemListDomain,
};
