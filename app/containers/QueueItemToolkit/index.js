/*
 *
 * QueueItemToolkit
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import IconButton from 'material-ui/IconButton';
import classNames from 'classnames';
import {IconsPanel, Item} from './styles';

export class QueueItemToolkit extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {isVisible} = this.props;
    const classes = classNames("left", {"hide": !isVisible});
    return (
        <IconsPanel className={classes}>
          <Item><IconButton iconClassName="mdi mdi-dots-vertical"/></Item>
          <Item><IconButton iconClassName="mdi mdi-check"/></Item>
          <Item><IconButton iconClassName="mdi mdi-pin"/></Item>
        </IconsPanel>
    );
  }
}

QueueItemToolkit.defaultProps = {
  isVisible: false
};

QueueItemToolkit.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isVisible: React.PropTypes.bool.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(QueueItemToolkit);
