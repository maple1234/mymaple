import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    direction: rtl;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    overflow-y: scroll;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  
  .container {
    margin: 0 auto;
    max-width: none;
    width: 85%;
  }

  @media only screen and (min-width: 601px)
    .container {
      width: 95%;
  }
  @media only screen and (min-width: 993px)
    .container {
      width: 90%;
  }

  .hide{
    display: none;
  }

  .left{
    float:left;
  }

  .right{
    float:right;
  }

  ul{
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
`;
