import { fromJS } from 'immutable';
import {
  FETCH_MULTIPLE_QUEUE_ITEMS_SUCCESS,
  FETCH_QUEUE_LABELS_SUCCESS
  } from '../constants';
import {
  SET_ITEM_IS_READ_SUCCESS,
  SET_ITEM_IS_UNREAD_SUCCESS,
  SET_ITEM_IS_DONE_SUCCESS,
  SET_ITEM_IS_UNDONE_SUCCESS,
  SET_ITEM_IS_PINNED_SUCCESS,
  SET_ITEM_IS_UNPINNED_SUCCESS
} from 'containers/QueueItem/constants';

const initialState = fromJS({queueItemsData : [], queueLabels: []});

function dataReducer(state = initialState, action){
    switch (action.type) {
    case FETCH_MULTIPLE_QUEUE_ITEMS_SUCCESS: 
      return state.set('queueItemsData', state.get('queueItemsData').merge(fromJS(action.multipleQueueItems)));
    case FETCH_QUEUE_LABELS_SUCCESS:
      return state.set('queueLabels', action.queueLabels);
    case SET_ITEM_IS_READ_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isRead', true);
    case SET_ITEM_IS_UNREAD_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isRead', false);
    case SET_ITEM_IS_DONE_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isDone', true);
    case SET_ITEM_IS_UNDONE_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isDone', false);
    case SET_ITEM_IS_PINNED_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isPinned', true);
    case SET_ITEM_IS_UNPINNED_SUCCESS: 
      return changeSingleQueueItemState(state, action.externalId, 'isPinned', false);
    default:
      return state;
  }
}

function changeSingleQueueItemState(state, externalId, field, value){
    const itemIndex = state.get('queueItemsData').findIndex((item)=> item.get('externalId') === externalId);
    return state.setIn(['queueItemsData', itemIndex, field], value);
}

export default dataReducer;
