import React from 'react';
import styled from 'styled-components';

export const IconsPanel = styled.ul`
 z-index: 10;
 position: relative;
 margin: 0px 5px;
`;

export const Item = styled.li`
 float: left;
`;