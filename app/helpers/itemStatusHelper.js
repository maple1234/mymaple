export function changeItemStatusAction(constant){
  return (externalId) => { return {type: constant, externalId}; };
}

export function changeOverallStatus(constant){
  return ()=> { return {type: constant}; };
}

export function changeItemStatusActionFailed(constant){
  return (message) => { return {type: constant, message}; };
}
