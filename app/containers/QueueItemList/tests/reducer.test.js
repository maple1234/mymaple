
import { fromJS } from 'immutable';
import queueItemListReducer from '../reducer';

describe('queueItemListReducer', () => {
  it('returns the initial state', () => {
    expect(queueItemListReducer(undefined, {})).toEqual(fromJS({}));
  });
});
