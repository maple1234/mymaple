/**
*
* Container
*
*/

import React from 'react';
// import styled from 'styled-components';


function Container({children}) {
  return (
    <div className="container">
      {children}
    </div>
  );
}

Container.propTypes = {
    children: React.PropTypes.node,
};

export default Container;
